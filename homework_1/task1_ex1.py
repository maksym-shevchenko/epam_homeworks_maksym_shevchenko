"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""

import argparse

def calculate(args):
    operations = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y,
    }

    if args.expr[1] not in operations:
        raise NotImplementedError
    return operations[args.expr[1]](float(args.expr[0]), float(args.expr[2]))


def main():
    parser = argparse.ArgumentParser(description='Process basic \
        arithmetic operations')
    parser.add_argument(
        'expr', nargs='+',
        help='operand operation operand (example: 1 + 2)')

    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
