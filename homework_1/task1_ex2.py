"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""

import argparse
import operator
import math

def calculate(args):
    if 'attrgetter' in args.expr:
        return operator.attrgetter(2.0)
    else:
        try:
            getter = operator.attrgetter(args.expr[0])
            return getter(math)(*[float(arg) for arg in args.expr[1:]])
        except AttributeError:
            return getter(operator)(*[float(arg) for arg in args.expr[1:]])


def main():
    parser = argparse.ArgumentParser(description='Process standard \
        math functions')
    parser.add_argument(
        'expr', nargs='+',
        help='operation operand operand (example: add 1 2)')

    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
