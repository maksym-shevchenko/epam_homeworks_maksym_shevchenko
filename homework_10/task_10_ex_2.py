"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
['donec', 'etiam', 'aliquam']
: Remember about dots, commas, capital letters etc.
"""

import re

def most_common_words(file_path: str, top_words: int) -> list:
    with open(file_path) as reader:
        words = re.findall('\w+', reader.read().lower())
    word_count = {}
    for w in words:
        word_count[w] = word_count.setdefault(w, 0) + 1
    res = sorted(word_count.keys(), key=lambda w: word_count[w], reverse=True)
    return res[:top_words]
