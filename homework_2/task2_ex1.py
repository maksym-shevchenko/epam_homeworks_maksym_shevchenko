"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""

import argparse

def bounded_knapsack(args):
    if len(args.w) != args.n or min(args.w) < 0 or args.W < 0:
        raise ValueError

    if sum(args.w) <= args.W:
        return sum(args.w)

    # All possible combinations of gold bars
    combinations = [[]]
    for w in args.w:
        combinations += [x+[w] for x in combinations]

    weights = [sum(x) for x in combinations]
    return max(filter(lambda x: x <= args.W, weights))


def main():
    parser = argparse.ArgumentParser(description='Returns the maximum weight of gold \
         that fits into a knapsack\'s capacity')
    parser.add_argument('-W', type=int, nargs='?',
                        help='integer describing the capacity of a knapsack')
    parser.add_argument('-w', nargs='+', type=int,
                        help='list of weights of each gold bar')
    parser.add_argument('-n', type=int, nargs='?',
                        help='integer describing the number of gold bars')

    args = parser.parse_args()
    print(bounded_knapsack(args))


if __name__ == '__main__':
    main()
