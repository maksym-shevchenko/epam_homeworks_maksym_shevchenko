"""
Write a function converting a Roman numeral from a given string N into an Arabic numeral.
Values may range from 1 to 100 and may contain invalid symbols.
Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""

import argparse

def from_roman_numerals(args):
    numbers = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
    }
    flag = 1
    previous_number = 1
    converted_number = 0

    for n in args.N[::-1]:
        if n not in numbers:
            raise ValueError
        current_number = numbers[n]
        if current_number > previous_number:
            flag = 1
        elif current_number < previous_number:
            flag = -1
        converted_number += flag * current_number
        previous_number = current_number
    
    if converted_number > 100:
        raise ValueError
    
    return converted_number



def main():
    parser = argparse.ArgumentParser(description='Convert a Roman numeral \
        from a given string N into an Arabic numeral')
    parser.add_argument('N', help='Roman numeral that we need to convert')

    args = parser.parse_args()
    print(from_roman_numerals(args))


if __name__ == "__main__":
    main()
