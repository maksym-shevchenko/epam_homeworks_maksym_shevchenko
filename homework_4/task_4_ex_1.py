"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""
def my_replace(string: str, change: str, to: str) -> str:
    return to.join(string.split(change))

def swap_quotes(string: str) -> str:
    string = my_replace(string, '\"', 'ї')
    string = my_replace(string, '\'', '\"')
    return my_replace(string, 'ї', '\'')
