"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""

def split_alternative(str_to_split: str, delimiter='') -> list:
    if not (isinstance(str_to_split, str) and isinstance(delimiter, str) \
            and str_to_split and delimiter):
        raise ValueError
    res = []
    delim_len = len(delimiter)
    while True:
        ind = str_to_split.find(delimiter)
        if ind == -1:
            res.append(str_to_split)
            break
        res.append(str_to_split[:ind])
        str_to_split = str_to_split[ind+delim_len:]
    return res