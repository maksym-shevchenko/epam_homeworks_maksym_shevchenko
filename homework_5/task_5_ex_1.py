"""
Write 2 functions:
1. Function 'is_sorted', determining whether a given list of integer values of arbitrary length
is sorted in a given order (the order is set up by enum value SortOrder).
List and sort order are passed by parameters. Function does not change the array, it returns
boolean value.

2. Function 'transform', replacing the value of each element of an integer list with the sum
of this element value and its index, only if the given list is sorted in the given order
(the order is set up by enum value SortOrder). List and sort order are passed by parameters.
To check, if the array is sorted, the function 'is_sorted' is called.

Example for 'transform' function,
For [5, 17, 24, 88, 33, 2] and “ascending” sort order values in the array do not change;
For [15, 10, 3] and “ascending” sort order values in the array do not change;
For [15, 10, 3] and “descending” sort order the values in the array are changing to [15, 11, 5]

Note:
Raise TypeError in case of wrong function arguments data type;
"""

from enum import Enum

class SortOrder(Enum):
    ASC = 'ascending'
    DSC = 'descending'

def check_is_correct_data(num_list: list[int], sort_order: SortOrder) -> bool:
    if not isinstance(sort_order, SortOrder) or \
        not (sort_order.value==SortOrder.ASC.value or
            sort_order.value==SortOrder.DSC.value) or \
                not isinstance(num_list, list):
        raise TypeError
    for n in num_list:
        if not isinstance(n, int) or isinstance(n, bool):
            raise TypeError
    
def is_sorted(num_list: list[int], sort_order: SortOrder) -> bool:
    check_is_correct_data(num_list, sort_order)
    if sort_order.value == SortOrder.DSC.value:
        num_list = num_list[::-1]
    for i in range(1, len(num_list)):
        if num_list[i] < num_list[i-1]:
            return False
    return True

def transform(num_list: list[int], sort_order: SortOrder) -> list[int]:
    check_is_correct_data(num_list, sort_order)
    if is_sorted(num_list, sort_order):
        for i in range(len(num_list)):
            num_list[i] += i
    return num_list
