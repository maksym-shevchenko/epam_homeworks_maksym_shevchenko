"""
Create CustomList – the linked list of values of random type,
which size changes dynamically and has an ability to index
elements.

The task requires implementation of the following functionality:
• Create the empty user list and the one based on enumeration of
values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be
implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first
                occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the
    object, of course).
    Function names should be as described above. Additional functionality
    has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list,
    starting with link to head.
"""


class Item:
    """
    A node in a unidirectional linked list.
    """
    def __init__(self, data) -> None:
        self.data = data
        self.next = None

class CustomList:
    """
    An unidirectional linked list.
    """
    def __init__(self, *data) -> None:
        self.head = None
        if data:
            item = Item(data[0])
            self.head = item
            for i in data[1:]:
                item.next = Item(i)
                item = item.next

    def append(self, value) -> None:    #   to add to the end
        if self.head is None:
            self.head = Item(value)
        else:
            item = self.head
            while item.next is not None:
                item = item.next
            item.next = Item(value)

    def add_start(self, value) -> None: #   to add to the start
        if self.head is None:
            self.head = Item(value)
        else:
            item = Item(value)
            item.next = self.head
            self.head = item

    def remove(self, value) -> None:    #   to remove the first occurrence of given value
        if self.head is None:
            raise KeyError
        if self.head.data == value:
            self.head = self.head.next
        else:
            prev = self.head
            item = prev.next
            while item is not None:
                if item.data == value:
                    prev.next = item.next
                    return None
                prev = item
                item = item.next
            raise KeyError
    
    #   Operations with elements by index. Negative indexing is not necessary.
    def __getitem__(self, index) -> Item:
        item = self.head
        if item is None:
            raise IndexError
        ind = 0
        while item is not None:
            if ind == index:
                return item.data
            ind += 1
            item = item.next
        raise IndexError

    def __setitem__(self, index, data) -> None:
        item = self.head
        if item is None:
            raise IndexError
        ind = 0
        while item is not None:
            if ind == index:
                item.data = data
                return None
            ind += 1
            item = item.next
        raise IndexError

    def __delitem__(self, index) -> None:
        if self.head is None:
            raise IndexError
        if index == 0:
            self.head = self.head.next
        else:
            ind = 1
            prev = self.head
            item = prev.next
            while item is not None:
                if ind == index:
                    prev.next = item.next
                    return None
                ind += 1
                prev = item
                item = item.next
            raise IndexError

    def find(self, value) -> int:   #   Receive index of predetermined value
        if self.head is None:
            raise KeyError
        if self.head.data == value:
            return 0
        else:
            prev = self.head
            item = prev.next
            ind = 1
            while item is not None:
                if item.data == value:
                    return ind
                ind += 1
                prev = item
                item = item.next
            raise KeyError

    def clear(self) -> None:    #   Clear the list
        self.head = None

    def __len__(self) -> int:   #   Receive lists length
        item = self.head
        ind = 0
        while item is not None:
            ind += 1
            item = item.next
        return ind

    def __iter__(self): #   Make CustomList iterable to use in for-loops
        item = self.head
        while item is not None:
            yield item
            item = item.next
