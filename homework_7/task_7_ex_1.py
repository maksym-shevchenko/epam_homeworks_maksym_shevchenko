"""
Develop Rectangle class with following content:
    2 private fields type of float `side_a` and `side_b` (sides А and В of the rectangle);
    One constructor with two optional parameters a and b (parameters specify rectangle sides).
    Side А of a rectangle
    defaults to 4, side В - 3. Raise ValueError if received parameters are less than or equal to 0;
    Method `get_side_a`, returning value of the side А;
    Method `get_side_b`, returning value of the side В;
    Method `area`, calculating and returning the area value;
    Method `perimeter`, calculating and returning the perimeter value;
    Method `is_square`, checking whether current rectangle is square or not. Returns True if the shape is square and
    False in another case;
    Method `replace_sides`, swapping rectangle sides.

Develop class ArrayRectangles, in which declare:
    Private attribute `rectangle_array` (list of rectangles);
    One constructor that creates a list of rectangles with length `n` filled with `None` and that receives an
    arbitrary amount of objects of type `Rectangle` or a list of objects of type `Rectangle` (the list must be
    unpacked inside the constructor so that there will be no nested arrays). If both objects and length are passed,
    at first creates a list with received objects and then add the required number of Nones to achieve the
    desired length. If `n` is less than the number of received objects, the length of the list will be equal to the
    number of objects;
    Method `add_rectangle` that adds a rectangle of type `Rectangle` to the array on the nearest free place and
    returning True, or returning False, if there is no free space in the array;
    Method `number_max_area`, that returns order number (index) of the first rectangle with the maximum area value
    (numeration starts from zero);
    Method `number_min_perimeter`, that returns order number (index) of the first rectangle with the minimum area value
    (numeration starts from zero);
    Method `number_square`, that returns the number of squares in the array of rectangles
"""


class Rectangle:
    __side_a = 4.0
    __side_b = 3.0

    def __init__(self, a: float=4, b: float=3) -> None:
        if a<=0 or b<=0:
            raise ValueError
        self.__side_a = a
        self.__side_b = b
    
    def get_side_a(self):
        return self.__side_a
    
    def get_side_b(self):
        return self.__side_b

    def area(self):
        return self.__side_a * self.__side_b
    
    def perimeter(self):
        return 2 * (self.__side_a + self.__side_b)
    
    def is_square(self):
        return self.__side_a == self.__side_b
    
    def replace_sides(self):
        self.__side_a, self.__side_b = self.__side_b, self.__side_a

class ArrayRectangles:
    __rectangle_array = []

    def __init__(self, *rectangles, n=0) -> None:
        self.__rectangle_array = []
        if rectangles:
            for rec in rectangles:
                if isinstance(rec, list):
                    self.__rectangle_array.extend(rec)
                else:
                    self.__rectangle_array.append(rec)
        while len(self.__rectangle_array) < n:
            self.__rectangle_array.append(None)
        
    def add_rectangle(self, rectangle: Rectangle) -> bool:
        if None not in self.__rectangle_array:
            return False
        self.__rectangle_array[self.__rectangle_array.index(None)] = rectangle
        return True
    
    def number_max_area(self) -> int:
        if self.__rectangle_array[0] is None:
            raise ValueError
        max_ind = -1
        max_area = -1
        ind = -1
        for rect in self.__rectangle_array:
            if rect is None:
                break
            ind += 1
            if rect.area() > max_area:
                max_area = rect.area()
                max_ind = ind
        return max_ind
        
    def number_min_perimeter(self) -> int:
        if self.__rectangle_array[0] is None:
            raise ValueError
        min_ind = 0
        min_perim = self.__rectangle_array[0].perimeter()
        ind = 0
        for rect in self.__rectangle_array[1:]:
            if rect is None:
                break
            ind += 1
            if rect.perimeter() < min_perim:
                min_perim = rect.perimeter()
                min_ind = ind
        return min_ind

    def number_square(self):
        return len(list(filter(lambda x: x is not None and x.is_square(), \
                self.__rectangle_array)))
