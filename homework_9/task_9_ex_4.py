"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
Note: use `string.ascii_lowercase` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
test_strings = ["hello", "world", "python", ]
print(chars_in_all(*test_strings))
{'o'}
print(chars_in_one(*test_strings))
{'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(chars_in_two(*test_strings))
{'h', 'l', 'o'}
print(not_used_chars(*test_strings))
{'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}
"""

import string
import re

def check_data(*strings):
    for s in strings:
        if not isinstance(s, str):
            raise TypeError

def chars_in_all(*strings):
    check_data(*strings)
    if not strings:
        return set()
    
    characters_in_each_string = [set(re.sub('\W', '', s).lower())
                                            for s in strings]
    return set.intersection(*characters_in_each_string)


def chars_in_one(*strings):
    check_data(*strings)
    if not strings:
        return set()
    
    characters_in_each_string = [set(re.sub('\W', '', s).lower())
                                            for s in strings]
    return set.union(*characters_in_each_string)


def chars_in_two(*strings):
    check_data(*strings)
    if len(strings) < 2:
        raise ValueError
    
    res = set()
    buf = set()
    characters_in_each_string = [set(re.sub('\W', '', s).lower())
                                            for s in strings]
    for s in characters_in_each_string:
        res.update(buf & s)
        buf.update(s)
    return res


def not_used_chars(*strings):
    check_data(*strings)
    return set(string.ascii_lowercase) ^ chars_in_one(*strings)
